const POKE_NAME_QUERY = `query samplePokeAPIquery($ids: [Int!]) {
	pokemon_v2_pokemon(where: {id: {_in: $ids}}) {
		name
		id
	}
}`;
const API_URL = "https://beta.pokeapi.co/graphql/v1beta";
const SPRITE_URL_BASE = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/";
const POKEMON_COUNT = 1020;

let name_cache = {};

function getAge(birthDate) {
	var today = new Date();
	var age = today.getFullYear() - birthDate.getFullYear();
	var m = today.getMonth() - birthDate.getMonth();
	if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
		age--;
	}
	return age;
}

async function getNames(ids) {
	// Remove already fetched
	ids = ids.filter(id => id <= POKEMON_COUNT && !(id in name_cache));
	// Only send a request if there are unknown ids
	if (ids.length > 0) {
		console.log(ids);
		res_json = await fetch(API_URL, {
			method: "POST",
			body: JSON.stringify({
				query: POKE_NAME_QUERY,
				variables: {ids: ids}
			})
		}).then(r => r.json());
		// id => name
		fetched_names = Object.fromEntries(res_json.data.pokemon_v2_pokemon.map(x => [x.id, x.name]));
		// Update cache
		name_cache = {...name_cache, ...fetched_names};
	}
	return name_cache;
}

function spriteUrl(id) {
	return `${SPRITE_URL_BASE}${id}.png`
}

document.getElementById("date").addEventListener("keypress", function (event) {
	// If the user presses the "Enter" key on the keyboard
	if (event.key === "Enter") {
		// Cancel the default action, if needed
		event.preventDefault();
		// Trigger the button element with a click
		document.getElementById("submit").click();
	}
});

async function submitClicked() {
	try {
		let birthDate = new Date(document.getElementById("date").value);
		let age = getAge(birthDate);
		let month = birthDate.getMonth() + 1;
		let day = birthDate.getDate();
		let pokemonIds = Array.from({length: 150}, (_, x) => (x + day) * month)
		let pokemonNames = await getNames(pokemonIds);

		let currentId = (age + day) * month;

		let currentPokemon = pokemonNames[currentId];
		document.getElementById(
			"resultText"
		).innerHTML = `Your current pokemon is ${currentPokemon}`;
		document.getElementById(
			"result"
		).innerHTML = `<img src="${spriteUrl(currentId)}">`;

		document.getElementById("resultListText").innerHTML =
			"<h4>Here are your life pokemons</h4>";
		document.getElementById("resultList").innerHTML = "";

		for (let [index, pokemonId] of pokemonIds.slice(1).entries()) {
			let pokemonName = pokemonNames[pokemonId];
			if (pokemonName === undefined) break;

			document.getElementById(
				"resultList"
			).innerHTML += `<div style="display:flex; flex-direction:column;"><h4>${index+1} YO : <br> ${
				pokemonName
			}</h4><img width="100px" src="${spriteUrl(pokemonId)}"></div>`;
		}
	}
	catch {
		console.log("oopsie");
	}
}
