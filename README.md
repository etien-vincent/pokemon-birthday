# pokemon-birthday

Quick and dirty website to display what is your pokemon, using (age + day of birth) * month of birth.
It displays your current pokemon as well as the pokemons for all your birthdays.
I developped this in  approximately an hour to save calculation and googling time to some friends.

Available at https://etien-vincent.gitlab.io/pokemon-birthday/
